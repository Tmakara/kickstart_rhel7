## Making the Kickstart File Available
#==========================================
- Check Kistart File
# yum install pykickstart
# ksvalidator -v RHEL7 ks.cfg 
- Mount the ISO image you have downloaded:
# mount -o loop /path/to/image.iso /mnt/iso
- Extract and Copy source of the ISO image into a working directory somewhere in your system:
# cp -pRf /mnt/iso /tmp/workdir
- Unmount the mounted image:
# umount /mnt/iso
- The contents of the image is now placed in the workdir/ directory in your working directory. Add your Kickstart file (ks.cfg) into source of the ISO image:
# cp /path/to/ks.cfg /tmp/workdir/

- Open the isolinux/isolinux.cfg configuration file inside the iso/ directory. This file determines all the menu options which appear in the boot menu. A single menu entry is defined as the following:
# vim /tmp/workdir/isolinux/isolinux.cfg
    abel linux
    menu label ^Install or upgrade an existing system
    menu default
    kernel vmlinuz
    append initrd=initrd.img  inst.ks=installer

*** Note: Add the inst.ks=installer to boot option to the line beginning with append. The exact syntax depends on how you plan to boot the ISO image; for example, if you plan on booting from a CD or DVD, use ks=cdrom:/ks.cfg

- On systems using the GRUB2 boot loader (AMD64, Intel 64, and 64-bit ARM systems with UEFI firmware and IBM Power Systems servers), the file name will be grub.cfg, so we need to modify this as well.
# vim /tmp/workdir/EFI/BOOT/grub.cfg
    
    linuxefi /images/pxeboot/vmlinuz inst.ks=installer

*** Note: Add the inst.ks=installer to pxe option to the after line of vmlinuz

- Use genisoimage in the iso/ directory to create a new bootable ISO image with your changes included:

    genisoimage -U -r -v -T -J -joliet-long -V "RHEL-X.X Server.x86_64" -volset "RHEL-X.X Server.x86_64" -A "RHEL-X.X Server.x86_64" -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -eltorito-alt-boot -e images/efiboot.img -no-emul-boot -o ../NEWISO.iso .

- Implant a md5 checksum into the new ISO image:
# dnf install isomd5sum
# implantisomd5 ../NEWISO.iso
