#!/bin/bash
#VERSION=CentOS/RedHat/Fedora/OracleLinux
#Color Variable
GREEN='\033[0;32m'
WHITE='\033[0;37m'
BIBlue='\033[1;94m'
UBlue='\033[4;34m'
Color_Off='\033[0m'
#Command Variable
ks_files=ks.cfg
ftp_directory=/var/ftp/pub
ftp_cfg_files=/etc/vsftpd/vsftpd.conf

echo -e "Processing FTP installation on ${WHITE}$(date)" >> /etc/issue
#Update if Required
#yum update -y
yum install vsftpd -y 
cp $ks_files $ftp_directory
cp $ftp_cfg_files "$ftp_cfg_files.$(date +%F_%R)"
#Import configuration to vsftpd files configuration
cat <<EOF> $ftp_cfg_files
anonymous_enable=YES
local_enable=YES
write_enable=YES
local_umask=022
anon_upload_enable=YES
anon_mkdir_write_enable=YES
dirmessage_enable=YES
xferlog_enable=YES
connect_from_port_20=YES
#chown_uploads=YES
#chown_username=whoever
listen=YES
pam_service_name=vsftpd
userlist_enable=YES
EOF
#Enable FTP services as a permanent
systemctl enable --now vsftpd 
sed -i 's/IPTABLES_MODULES=""/IPTABLES_MODULES="ip_conntrack_ftp"/g' /etc/sysconfig/iptables-config
iptables -I INPUT -p tcp -m tcp --dport 20 -j ACCEPT
iptables -I INPUT -p tcp -m tcp --dport 21 -j ACCEPT
# Save and restart
iptables-save | tee "/tmp/iptables_log.$(date +%F_%R)" > /dev/null
#allow selinux
echo -e " \n Allow Selinux Process for FTP \n ${GREEN}selinux applied...!! ${Color_Off}"
setsebool -P allow_ftpd_anon_write on
#allow permission
echo -e " \n Applying permission to public directory FTP \n ${GREEN}Permission applied...!! ${Color_Off}"
chown ftp: -R $ftp_directory
chown 755 -R $ftp_directory
chcon -t public_content_rw_t $ftp_directory
#Restart FTP Service
systemctl restart vsftpd 2>&1 | tee /tmp/ftp_log_restarted.txt
#verify ftp is working properly
echo -e "Now FPT is working fine with configure below: \n ${WHITE}$(curl ftp://localhost/pub/ks.cfg) ${Color_Off}"
echo -e " \n \n ${GREEN}FTP services is running... \n \n Now you can access FTP services via below URL: \n ${BIBlue}curl ${BIBlue}${UBlue}ftp://$(hostname -I | awk '{print $1}')/pub/ks.cfg ${Color_Off}"
