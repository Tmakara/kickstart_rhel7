#!/bin/bash
#VERSION=CentOS/RedHat/Fedora/OracleLinux
set -e

GREEN='\033[0;32m'
WHITE='\033[0;37m'
BIBlue='\033[1;94m'
UBlue='\033[4;34m'
Color_Off='\033[0m'
ks_files="ks.cfg"
http_directory="/var/www/html"
YUM_PACKAGE_NAME="httpd"
DEB_PACKAGE_NAME="apache2"

 if cat /etc/*release | grep ^NAME | grep CentOS; then
    echo "==============================================================================="
    echo -e "Installing packages $YUM_PACKAGE_NAME on CentOS ${WHITE}$(date) ${Color_Off}"
    echo "==============================================================================="
    yum install -y $YUM_PACKAGE_NAME 
    cp ks-templates/$ks_files $http_directory
#Allow Security Firewall
    firewall-cmd --permanent --zone=public --add-service=http
    firewall-cmd --reload
    iptables -I INPUT -p tcp -m tcp --dport 80 -j ACCEPT
# Save
    iptables-save | tee "/tmp/iptables_log.$(date +%F_%R)" > /dev/null
#allow selinux
    echo -e "Allow Selinux Process for $YUM_PACKAGE_NAME \n ${GREEN}selinux applied...!! ${Color_Off}"
    setsebool -P allow_httpd_anon_write on
#allow permission
    echo -e "\nApplying permission to public directory HTTPD \n${GREEN}Permission applied...!! ${Color_Off}"
    chown apache: -R $http_directory
    chmod 700 -R $http_directory
#Restart HTTPD Service
    systemctl restart $YUM_PACKAGE_NAME 2>&1 | tee /tmp/ftp_log_restarted.txt
#verify HTTP is working properly
    echo "=============================================="
    echo "$YUM_PACKAGE_NAME is working fine with configure below:"
    echo -e "${WHITE}$(curl http://localhost/ks.cfg) ${Color_Off}"
    echo "=============================================="
    echo -e "${GREEN}Your Web Service is running on port 80...${Color_Off}"
    echo "Now you can access web services via below URL:"
    echo -e "${BIBlue}${UBlue}http://$(hostname -I | awk '{print $1}')/ks.cfg ${Color_Off}"
    echo "=============================================="
 elif cat /etc/*release | grep ^NAME | grep Red; then
    echo "==============================================================================="
    echo -e "Installing packages $YUM_PACKAGE_NAME on RedHat ${WHITE}$(date) ${Color_Off}"
    echo "==============================================================================="
    yum install -y $YUM_PACKAGE_NAME 
    cp ks-templates/$ks_files $http_directory
#Allow Security Firewall
    firewall-cmd --permanent --zone=public --add-service=http
    firewall-cmd --reload
    iptables -I INPUT -p tcp -m tcp --dport 80 -j ACCEPT
# Save
    iptables-save | tee "/tmp/iptables_log.$(date +%F_%R)" > /dev/null
#allow selinux
    echo -e "Allow Selinux Process for $YUM_PACKAGE_NAME \n ${GREEN}selinux applied...!! ${Color_Off}"
    setsebool -P allow_httpd_anon_write on
#allow permission
    echo -e " \nApplying permission to public directory HTTPD \n ${GREEN}Permission applied...!! ${Color_Off}"
    chown apache: -R $http_directory
    chmod 700 -R $http_directory
#Restart HTTPD Service
    systemctl restart $YUM_PACKAGE_NAME 2>&1 | tee /tmp/ftp_log_restarted.txt
#verify HTTP is working properly
    echo "=============================================="
    echo "$YUM_PACKAGE_NAME is working fine with configure below:"
    echo -e "${WHITE}$(curl http://localhost/ks.cfg) ${Color_Off}"
    echo "=============================================="
    echo -e "${GREEN}Your Web Service is running on port 80...${Color_Off}"
    echo "Now you can access web services via below URL:"
    echo -e "${BIBlue}${UBlue}http://$(hostname -I | awk '{print $1}')/ks.cfg ${Color_Off}"
    echo "=============================================="
 elif cat /etc/*release | grep ^NAME | grep Ubuntu; then
    echo "==============================================================================="
    echo -e "Installing packages $DEB_PACKAGE_NAME on Ubuntu ${WHITE}$(date) ${Color_Off}"
    echo "==============================================================================="
    apt update
    apt install -y $DEB_PACKAGE_NAME
    cp ks-templates/$ks_files $http_directory
#Allow Security Firewall
    ufw allow 'Apache'
#allow permission
    echo -e " \n Applying permission to public directory HTTPD \n ${GREEN}Permission applied...!! ${Color_Off}"
    chown www-data: -R $http_directory
    chmod 700 -R $http_directory
#Restart HTTPD Service
    systemctl restart $DEB_PACKAGE_NAME 2>&1 | tee /tmp/ftp_log_restarted.txt
#verify HTTP is working properly
    echo "=============================================="
    echo "$YUM_PACKAGE_NAME is working fine with configure below:"
    echo -e "${WHITE}$(curl http://localhost/ks.cfg) ${Color_Off}"
    echo "=============================================="
    echo -e "${GREEN}Your Web Service is running on port 80...${Color_Off}"
    echo "Now you can access web services via below URL:"
    echo -e "${BIBlue}${UBlue}http://$(hostname -I | awk '{print $1}')/ks.cfg ${Color_Off}"
    echo "=============================================="
 else
    echo "OS NOT DETECTED, couldn't install package $PACKAGE"
    exit 1;
 fi

exit 0
